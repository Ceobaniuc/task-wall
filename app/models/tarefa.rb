class Tarefa < ApplicationRecord
  enum prioridade: { baixa: 0, media: 1, alta: 2}
  enum atribuicao: [:icaro, :pedro, :dhiego, :yago]
  enum situacao: { nova: 0, feita: 1}
  enum porcetagem_terminado: [ '0%',  '10%',  '20%',  '30%',  '40%',  '50%',  '60%',  '70%',  '80%',  '90%',  '100%' ]

  # validades :
end
