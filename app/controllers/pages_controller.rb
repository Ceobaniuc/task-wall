class PagesController < ApplicationController
  def novas_tarefas
    @tarefas = Tarefa.select { |a| a.situacao == 'nova'}
  end

  def tarefas_feitas
    @tarefas = Tarefa.select { |a| a.situacao == 'feita'}

    # Nota: Condição abaixo seleciona o method e mapeia o mesmo usando recude
    # object.select{|r| r.method?}.map(&:method)&.reduce(&:+)&.uniq || []
  end

  def home

  end
end
