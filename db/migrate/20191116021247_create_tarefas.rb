class CreateTarefas < ActiveRecord::Migration[5.2]
  def change
    create_table :tarefas do |t|
      t.string :titulo
      t.text :descricao
      t.integer :situacao
      t.integer :prioridade
      t.integer :atribuicao
      t.string :inicio
      t.string :data_prevista
      t.string :tempo_estimado
      t.integer :porcetagem_terminado

      t.timestamps
    end
  end
end
