# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or create!d alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create!([" name: "Star Wars" ", " name: "Lord of the Rings" "])
#   Character.create!(name: "Luke", movie: movies.first)

# Tarefas
Tarefa.create!(
  titulo: "Protocolo",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrs standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum",
  situacao: 0,
  prioridade: 0,
  atribuicao: 0,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 0
)
Tarefa.create!(
  titulo: "Curso",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 1,
  prioridade: 1,
  atribuicao: 1,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 1
)
Tarefa.create!(
  titulo: "Cadastramento",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 0,
  prioridade: 2,
  atribuicao: 2,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 2
)
Tarefa.create!(
  titulo: "Processual",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 1,
  prioridade: 0,
  atribuicao: 3,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 3
)
Tarefa.create!(
  titulo: "Informativo",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 0,
  prioridade: 1,
  atribuicao: 0,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 3
)
Tarefa.create!(
  titulo: "Digitalização",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 1,
  prioridade: 2,
  atribuicao: 1,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas", porcetagem_terminado: 5
)
Tarefa.create!(
  titulo: "Fazer chamado",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 0,
  prioridade: 0,
  atribuicao: 2,
  inicio: "16/11/2019", data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 6
)
Tarefa.create!(
  titulo: "Solicitar peças",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 1,
  prioridade: 1,
  atribuicao: 3,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 7
)
Tarefa.create!(
  titulo: "Alteração de de informação",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 0, prioridade: 2,
  atribuicao: 0,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 8
)
Tarefa.create!(
  titulo: "Lembrete",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 1,
  prioridade: 0,
  atribuicao: 1, inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 9
)
Tarefa.create!(
  titulo: "Layout",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 0, prioridade: 1,
  atribuicao: 2,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 10
)
Tarefa.create!(
  titulo: "Problema de cadastro",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 1,
  prioridade: 2,
  atribuicao: 3,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 1
)
Tarefa.create!(
  titulo: "Limpeza",
  descricao:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 0, prioridade: 0,
  atribuicao: 0,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 2
)
Tarefa.create!(
  titulo: "Observação",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 1, prioridade: 1,
  atribuicao: 1,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 3
)
Tarefa.create!(
  titulo: "Marcar reunião",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 0, prioridade: 2,
  atribuicao: 2,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 3
)
Tarefa.create!(
  titulo: "Concertar parte do filtro",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 1,
  prioridade: 0,
  atribuicao: 3,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 5
)
Tarefa.create!(
  titulo: "Mudar calendario",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 0, prioridade: 1,
  atribuicao: 0,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 6
)
Tarefa.create!(
  titulo: "Mudar horario",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 1, prioridade: 2,
  atribuicao: 1,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 7
)
Tarefa.create!(
  titulo: "Trocar água",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 0, prioridade: 0,
  atribuicao: 2,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 8
)
Tarefa.create!(
  titulo: "Comprar café",
  descricao: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  situacao: 1, prioridade: 1,
  atribuicao: 3,
  inicio: "16/11/2019",
  data_prevista: "16/11/2020",
  tempo_estimado: "20 horas",
  porcetagem_terminado: 9
)
