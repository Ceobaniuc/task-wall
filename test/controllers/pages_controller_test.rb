require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get novas_tarefas" do
    get pages_novas_tarefas_url
    assert_response :success
  end

  test "should get tarefas_feitas" do
    get pages_tarefas_feitas_url
    assert_response :success
  end

end
