Rails.application.routes.draw do
  get 'pages/novas_tarefas'
  get 'pages/tarefas_feitas'
  get 'pages/home'
  resources :tarefas
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

root to: 'pages#home'
end
