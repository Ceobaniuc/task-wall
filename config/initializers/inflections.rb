ActiveSupport::Inflector.inflections do |inflect|
  inflect.irregular 'atribuicao', 'atribuicoes'
  inflect.irregular 'descricao', 'descricoes'
  inflect.irregular 'data_prevista', 'datas_previstas'
  inflect.irregular 'tempo_estimado', 'tempos_estimados'
  inflect.irregular 'porcetagem_terminado', 'porcetagens_terminados'
  inflect.irregular 'situacao', 'situacoes'
end
